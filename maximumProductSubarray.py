class Solution(object):
    def maxProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        -odd number of negatives ->ded
        -is integers so always gets larger in magnitude with each multiplication
        -either include till first negative, from last negative, or both to control the sign
        *didn't consider 0!
        
        -track maxSoFar and current
        -Track firstNeg, middle, and lastNeg
        -if middle > 0 pick both 
        -if middle < 0 pick first or last
        
        -when hit a zero, reset current, firstNeg, and lastNeg
        (treat as totally new problem and if max is greater after then thats the max)
        
        simpler:
        track max negative and max positive, if next number is 
        when you encounter a negative number, they swap values
        """
        maxProd = nums[0]
        currentNeg = 1
        currentPos = -1
        for i in range(len(nums)):
            if nums[i]<0:
                if currentNeg>0:
                    if currentPos<0:
                        currentNeg = nums[i]
                    else:
                        currentNeg = currentPos*nums[i]
                        currentPos = -1
                else:
                    if currentPos<0:
                        currentPos = currentNeg * nums[i]
                        currentNeg = nums[i]
                    else:
                        temp = currentNeg
                        currentNeg = currentPos*nums[i]
                        currentPos = temp*nums[i]
            elif nums[i]>0:
                if currentPos<0:
                    currentPos = nums[i]
                    currentNeg *= nums[i]
                else:
                    currentPos *= nums[i]
                    currentNeg *= nums[i]
            else:
                currentPos = -1
                currentNeg = 1
                if maxProd < 0:
                    maxProd = 0
            if currentPos > 0 and currentPos>maxProd:
                maxProd = currentPos
        return maxProd